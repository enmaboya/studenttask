<?php

Route::middleware(['auth'])->group(function () {
    Route::post('/image/{student}', 'StudentController@upload')->name('image.upload');
    Route::get('/list/{group}', 'GroupController@viewStudents')->name('view.students');
    Route::get('/main', 'MainController@index')->name('main.index');
    Route::resource('students', 'StudentController');
    Route::resource('groups', 'GroupController');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
