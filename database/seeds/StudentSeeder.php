<?php

use Illuminate\Database\Seeder;
use App\Models\Student;
use App\Models\Group;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function randomid()
    {
        $groups = Group::all();
        return $groups->random()->id;
    }

    public function run()
    {
    	$studentCount = 50;
        for ($i=0; $i < $studentCount; $i++) {
                factory(Student::class)->create([
                'group_id' => $this->randomid(),
        		]);
        }
    }
}
