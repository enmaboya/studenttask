<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;
use App\Models\Rating;
use App\Models\Student;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = Student::all();
        foreach ($students as $student) {
            factory(Rating::class, 3)->create([
                'student_id' => $student->id,
                ]);
        }
    }
}
