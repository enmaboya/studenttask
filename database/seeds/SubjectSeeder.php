<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;
use App\Models\Group;
use App\Models\Rating;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert(['name' => 'Русский язык',]);
        DB::table('subjects')->insert(['name' => 'Математика',]);
        DB::table('subjects')->insert(['name' => 'История',]);
        $subjCount = Subject::count();
        $groups = Group::all();
        $ratings = Rating::all();


        for ($i=0; $i < $subjCount; $i++) {
            foreach ($groups as $group) {
                $group->avg = array();
                foreach ($group->students as $student) {
                    $group->avg[]=$student->ratings->get($i)->points;
                }
                $count = count($group->avg);
                $sum = array_sum($group->avg);
                if ($count != null) {
                    switch ($i) {
                        case '0':
                            $group->avgrus = $sum / $count;
                            $group->save();
                            break;
                        case '1':
                            $group->avgmath = $sum / $count;
                            $group->save();
                            break;
                        case '2':
                            $group->avghist = $sum / $count;
                            $group->save();
                            break;
                    }
                }
            }
        }
        $value = 1;
        foreach ($ratings as $rating) {
            $rating->subject_id = $value;
            $rating->save();
            $value++;
            if ($value == 4) $value = 1;
        }


    }
}

