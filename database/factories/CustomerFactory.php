<?php

use Faker\Generator as Faker;
use App\Models\Group;
use App\Models\Student;
use App\Models\Rating;
use App\Models\Subject;

$factory->define(Group::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    return [
        'title' => $faker->city,
        'description' => $faker->unique()->word,
    ];
});

$factory->define(Student::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('ru_RU');
    return [
        'name' => $faker->name,
        'birthday' => $faker->date,
    ];
});

$factory->define(Rating::class, function (Faker $faker) {
    return [
        'points' => $faker->numberBetween(1, 5),
    ];
});
