@extends('includes._layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">*__*</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
               <a class="navbar-brand" href="{{ route ('main.index') }}" method="GET">
                    <h1 class="display-7">{{"Привет, ".Auth::user()->name}}</h1>
               </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
