@extends ('includes._layout')

@section ('content')
	<table class="table table-hover">
		<thead>
			<th>№ п\п</th>
			<th>Имя</th>
			<th>Дата рождения</th>
		</thead>
		<tbody>
			<tr>
				@foreach ($group->students as $student)
					<tr>
						<td>{{$loop->index+1}}</td>
						<td>{{$student->name}}</td>
						<td>{{$student->birthday}}</td>
					</tr>
				@endforeach
			</tr>
		</tbody>
	</table>
@endsection