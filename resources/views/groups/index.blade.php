@extends ('includes._layout')

@section ('filter')
	@include ('includes.filters.groupfilter')
@endsection
@php
    use App\Models\Group;
@endphp

@section ('content')

<table class="table table-sm">
<thead>
	<th>Название группы</th>
	<th>Описание</th>
	<th>Успеваемость</th>
	<th>Средняя оценка по русскому языку</th>
	<th>Средняя оценка по математике</th>
	<th>Cредняя оценка по истории</th>
	<th></th>
</thead>
<tbody>
	@include('includes.filters.groupsort')
	<tr>
		@foreach ($groups as $group)
			<tr>
				<td><div class="body">&nbsp;<b> {{ $group->title }} </b></div></td>
				<td><div class="body">&nbsp;<b> {{ $group->description }} </b></div></td>
				<td><div class="body">&nbsp;<b> {{ $group->groupPerformance(5, $subjCount) }}% </b></div></td>
				@for ($i = 0; $i < $subjCount; $i++)
					<td><i>{{round($group->avgRatingInGroup($i), 2)}}</i></td>
				@endfor

				@can('show', $group)
					<td>
						<form action = "{{ route ('groups.show', $group) }}" method = "GET">
							@csrf
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-trash"> Редактировать </i>
							</button>
						</form>
					</td>
				@endcan
				@can('delete', $group)
					<td>
						<form action="{{ route('groups.destroy', ['id' => $group->id]) }}" method="post">
							@csrf
							@method('delete')
							<button type="submit" class="btn btn-danger">
								<i class="fa fa-trash"> Удалить</i>
							</button>
						</form>
					</td>
				@endcan
				<td>
					<form action="{{ route('view.students', $group) }}" method="get">
						@csrf
					    <button type="submit" class="btn btn-outline-dark">
					    	🚻
				      </button>
				    </form>
				</td>
			</tr>
		@endforeach
</tbody>
</table>

<nav>
	<ul class="pagination">
		<div class="container" style="margin:0 30% 0 30%;;">{{$groups->links()}}</div>
	</ul>
</nav>

@can('create', Group::class)
	<form action = "{{ route ('groups.create') }}" method = "GET">
			{{ csrf_field() }}
		<div style="width: 300px">
			<div class="input-group-prepend"><span class="input-group-text">Добавить группу</span>
			<button class="btn btn-outline-success" type="submit">+</button></div>
		</div>
	</form>
@endcan

@endsection