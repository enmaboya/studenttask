<form class="form-inline my-2 my-lg-0">
    <tr>
        @for ($i = 0; $i < 6; $i++)
            <td>
                @if ($i == 2)
                    @continue
                @endif
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-12">
                            <form>
                                <button type="submit" name="sort" value={{"asc".$i}}>▲</button>
                                <button type="submit" name="sort" value={{"des".$i}}>▼</button>
                                <input type="hidden" name="title" value="{{ request()->title}}">
                                <input type="hidden" name="desk" value="{{ request()->desk}}">
                                <input type="hidden" name="rus" value="{{ request()->rus}}">
                                <input type="hidden" name="math" value="{{ request()->math}}">
                                <input type="hidden" name="hist" value="{{ request()->hist}}">
                            </form>
                        </div>
                    </div>
                </div>
            </td>
        @endfor
    </tr>
</form>