<form class="form-inline my-2 my-lg-0">
    <tr>
        @for ($i = 0; $i < 7; $i++)
            <td>
                @if ($i == 3)
                    @continue
                @endif
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-12">
                            <form>
                                <button type="submit" name="studsort" value={{"asc".$i}}>▲</button>
                                <button type="submit" name="studsort" value={{"des".$i}}>▼</button>
                                <input type="hidden" name="fio" value="{{ request()->fio}}">
                                <input type="hidden" name="birt" value="{{ request()->birt}}">
                                <input type="hidden" name="russtud" value="{{ request()->russtud}}">
                                <input type="hidden" name="mathstud" value="{{ request()->mathstud}}">
                                <input type="hidden" name="histstud" value="{{ request()->histstud}}">
                            </form>
                        </div>
                    </div>
                </div>
            </td>
        @endfor
    </tr>
</form>