@extends ('includes._layout')

@section ('filter')
	@include ('includes.filters.studentfilter')
@endsection
@php
    use App\Models\Student;
@endphp

@section ('content')
	<table class="table table-sm">
	<thead>
		<th>Полное имя</th>
		<th>Дата рождения</th>
		<th>Группа</th>
		<th>Успеваемость</th>
		<th>Усп. по русскому языку</th>
		<th>Усп. по математике</th>
		<th>Усп. по истории</th>
	</thead>
	<tbody>
	@include('includes.filters.studentsort')
	@foreach ($students as $student)
		{{dd($student->group)}}
		<tr>
			<td><div class="body">&nbsp; {{ $student->name }} </div></td>
			<td><div class="body">&nbsp; {{ $student->birthday }} </div></td>
			<td><div class="body">&nbsp; {{ $student->groups }} </div></td>
			<td><div class="body">&nbsp;<b> {{ $student->StudentPerformance($student, 5, $subjCount) }}% </b></div></td>
			@foreach ($student->ratings as $rating)
				<td><div class="body"> {{$student->performanceInSubjAsPercent($rating->points, 5)}}% </div></td>
			@endforeach

			@can('show', $student)
				<td>
					<form action = "{{ route ('students.show', $student) }}" method = "GET">
					  {{ csrf_field() }}
					  <button type="submit" class="btn btn-primary">
						<i class="fa fa-trash"> Анкета </i>
					  </button>
					</form>
				</td>
			@endcan
			@can('delete', $student)
				<td>
					<form action="{{ route('students.destroy', $student) }}" method="post">
						@csrf
						@method('delete')
						<button type="submit" class="btn btn-danger">
						<i class="fa fa-trash"> Удалить </i>
					  </button>
					</form>
				</td>
			@endcan
		</tr>
	@endforeach
	</tbody>
	</table>

	<nav>
		<ul class="pagination">
			<div class="container" style="margin:0 30% 0 30%;;">{{$students->links()}}</div>
		</ul>
	</nav>

	@can('create', Student::class)
		<form action = "{{ route ('students.create') }}" method = "GET">
			@csrf
		   <div style="width: 300px">
				 <div class="input-group-prepend"><span class="input-group-text">Добавить студента</span>
				 <button class="btn btn-outline-success" type="submit">+</button></div>
		   </div>
	   </form>
	@endcan
@endsection