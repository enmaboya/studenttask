@extends ('includes._layout')

@section ('content')
  <div class="card-deck">
  <div class="card col-3" >
    <div class="card-body">
      <h5 class="card-title">Фото</h5>
        @include ('students.upload')
    </div>
  </div>
  <div class="card col-5">
    <div class="card-body">
      <h5 class="card-title">Анкета студента</h5>
      <form action = "{{ route ('students.edit', $student) }}" method = "GET">
          <div>
            <div>
              <label for="disabledTextInput">ФИО</label>
              <input type="text" value="{{$student->name}}" placeholder="{{$student->name}}" name="name" style="width: 260px">
            </div>
            <div>
              <label for="disabledTextInput">Дата рождения</label>
              <input type="date" value="{{$student->birthday}}" placeholder="{{$student->birthday}}" name="birthday">
            </div>
            <div>
              <label for="disabledTextInput">Группа</label>
            <div>
              <select required class="custom-select" name="group_id" style="width: 300px">
                 @foreach ($groups as $group)
                 <option value="{{$group->id}}"> {{$group->title}} </option>
                 @endforeach
              </select>
            </div>
            <div>
              <label for="disabledTextInput">Оценки</label>
              <input type="text" value="{{$student->ratings->pluck('points')}}" name="points" disabled="true">
            </div>
          </div>
          <div class="input-group-prepend">
            <span class="input-group-text">Принять изменения</span>
            <button class="btn btn-outline-info" type="submit">~</button>
          </div>
        </form>
    </div>
  </div>
</div>
@endsection