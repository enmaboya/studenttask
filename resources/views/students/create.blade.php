@extends ('includes._layout')

@section ('content')
	<form action = "{{ route ('students.store') }}" method = "POST">
		 {{ csrf_field() }}
		<div style="width: 300px">
		  	<div class="input-group-prepend">
		  		<span class="input-group-text">Добавить студента</span>
		  	</div>
		 	<input type="text" aria-label="ФИО студента" class="form-control" placeholder="ФИО студента" name="name">
		  	<input type="date" aria-label="Дата рождения" class="form-control" placeholder="Введите дату рождения" name="birthday">
		  	<div>
			  	<select class="custom-select" id="inputGroupSelect01" name="group_id">
			  		<option disabled selected>Выберите группу</option>
				@foreach ($groups as $group)
					<option value="{{$group->id}}"> {{$group->title}} </option>
				@endforeach
				</select>
			</div>
		  	<input type="text" aria-label="Оценка по русскому языку" class="form-control" placeholder="Укажите оценку по русскому языку" name="rating-rus" required>
		  	<input type="text" aria-label="Оценка по математике" class="form-control" placeholder="Укажите оценку по математике" name="rating-math" required>
		  	<input type="text" aria-label="Оценка по истории" class="form-control" placeholder="Укажите оценку по истории" name="rating-hist" required>
		  	<div>
		  		<button class="btn btn-outline-success" type="submit">Добавить</button>
		  	</div>
		</div>
	</form>
@endsection