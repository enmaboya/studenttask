
@isset($student->imgpath)
	<div class="card" style="width: 10rem;">
  		<img class="card-img-top" src="{{ asset('/storage/'.$student->imgpath) }}">
	</div>
@endisset
<table>
	<tbody>
		<tr>
	        <td>
	          <form action="{{ route('image.upload', $student) }}" method="post" enctype="multipart/form-data">
	            {{ csrf_field() }}
	            <div class="input-group">
				  <div class="custom-file">
				    <input type="file" class="custom-file-input" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04" name="image">
				    <label class="custom-file-label" for="inputGroupFile04">🔍</label>
				  </div>
				  <div class="input-group-append">
				    <button class="btn btn-outline-secondary" type="submit" id="inputGroupFileAddon04">🡒</button>
				  </div>
				</div>
	          </form>
	        </td>
	    </tr>
	</tbody>
</table>