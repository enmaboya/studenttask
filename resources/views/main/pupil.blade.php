<table class="table table-striped table-light">
	<thead>
			<th>Отличники</th>
			<td>Русский язык</td>
			<td>Математика</td>
			<td>История</td>
	</thead>
	@foreach ($groups as $group)
		@foreach ($group->students as $student)
			@if ($student->ratings->avg('points') == 5)
			<tbody>
			<tr>
				<td width="150"><div class="student-body">{{ $student-> name }}</div></td>
					@foreach ($student->ratings as $rating)
				<td width="150"><div class="rating-body">{{ $rating->points }}</div></td>
					@endforeach
			</tbody></tr>
			@endif
		@endforeach
	@endforeach
</table>

<table class="table table-striped table-light">
	<thead>
			<th>Ударники</th>
			<td>Русский язык</td>
			<td>Математика</td>
			<td>История</td>
	</thead>
	@foreach ($groups as $group)
		@foreach ($group->students as $student)
			@if ($student->ratings->avg('points') >= 4.0 & $student->ratings->avg('points') < 5.0)
			<tbody>
			<tr>
				<td width="150"><div class="student-body">{{ $student-> name }}</div></td>
					@foreach ($student->ratings as $rating)
				<td width="150"><div class="rating-body">{{ $rating->points }}</div></td>
					@endforeach
			</tbody></tr>
			@endif
		@endforeach
	@endforeach
</table>