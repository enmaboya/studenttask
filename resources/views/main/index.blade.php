@extends ('includes._layout')

@section('navigate')
    @include('navbar')
@endsection

@section ('content')
	@foreach ($groups as $group)
			<tr><th><div class="body">&nbsp;<b> {{ $group->title }} </b></div>
		<table class="table table-bordered table-dark">
			<thead>
				<th>ФИО</th>
				<th>День рождения</th>
				<th>Русский язык</th>
				<th>Математика</th>
				<th>История</th>
				<th>Общее среднее</th>
			</thead>
			<tbody>

				@foreach ($group->students as $student)
					@php
						$active = ' ';
						$active = $student->colorHightlight($student->ratings->avg('points'));
					@endphp
					<tr><th width="200"><div class="student-body" {{$active}}>{{ $student-> name }}</div></th>
					<th width="150"><div class="student-birthday">{{ $student->birthday }}</div>
						@foreach ($student->ratings as $rating)
							<td width="150"><div class="rating-body">{{ $rating->points }}</div></td>
						@endforeach
						<td> {{round($student->ratings->avg('points'),2)}} </td>
				@endforeach
					<tr>
						<td width="250">Средние баллы по предметам</td><td></td>
						<td><i>{{round($group->avgRatingInGroup(0),2)}}</i></td>
						<td><i>{{round($group->avgRatingInGroup(1),2)}}</i></td>
						<td><i>{{round($group->avgRatingInGroup(2),2)}}</i></td>
					</tr>
			</tbody>
		</table>
		<nav></nav>
	@endforeach
		<nav>
			<ul class="pagination">
				<div class="container" style="margin:0 30% 0 30%;;">{{$groups->links()}}</div>
			</ul>
		</nav>
		@include('main.pupil')
@endsection