<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $fillable = [
        'points',
        'student_id',
        'subject_id',
    ];

    public function students()
    {
        return $this->belongsTo(Student::class);
    }

    public function subjects()
    {
        return $this->belongsTo(Subject::class);
    }

    public static function averageSubj($groups)
    {
        $avg = collect();
        foreach ($groups as $group) {
            foreach ($group->students as $student) {
                    $avg->push($student->ratings->sum('points')/$student->ratings->count('points'));
            }
        }
        return $avg;
    }
}
