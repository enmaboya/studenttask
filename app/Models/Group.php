<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable  = [
        'title',
        'description'
    ];

    public $avg;

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function ratings()
    {
        return $this->hasManyThrough(Rating::class, Student::class);
    }

    public function avgRatingInGroup($mode)
    {
        $this->avg = array();
        foreach ($this->students as $student) {
            $this->avg[]=$student->ratings->get($mode)->points;
        }
        $count = count($this->avg);
        $sum = array_sum($this->avg);
        if ($count != null) {
            switch ($mode) {
                case '0':
                    $this->avgrus = $sum / $count;
                    $this->save();
                    break;
                case '1':
                    $this->avgmath = $sum / $count;
                    $this->save();
                    break;
                case '2':
                    $this->avghist = $sum / $count;
                    $this->save();
                    break;
            }
        }
        if ($count != null) {
            return $sum / $count;
        } else {
            return 0;
        }
    }

    public function groupPerformance($max, $subjCount)
    {
        $studCount = $this->students->count();
        $maxpoint = $max * $studCount * $subjCount;
        if ($maxpoint != null) {
            return round(($this->ratings->sum('points') / $maxpoint) * 100, 2);
        } else {
            return 0;
        }
    }

    public function scopeFilter($builder, $filters)
    {
        return $filters->apply($builder);
    }
}