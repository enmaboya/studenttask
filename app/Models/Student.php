<?php

namespace App\Models;

use App\Models\Group;
use App\Models\Student;
use App\Models\Rating;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'group_id',
        'name',
        'birthday'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function colorHightlight($value)
    {
        $active =" ";
        if ($value == 5.0) {
            $active = "style = color:green;";
        } elseif ($value <= 3.9) {
            $active = "style = color:red;";
        } elseif ($value >= 4.0 && $value < 5.0) {
            $active = "style = color:#FFE4B5;";
        }
        return $active;
    }

    public function StudentPerformance($student, $max, $subjCount)
    {
        $maxpoint = $max * $subjCount;
        if ($maxpoint != null) {
            $result = round(($student->ratings->sum('points') / ($max * $subjCount)) * 100, 2);
            return $result;
        } else {
            return 0;
        }
    }
    public function performanceInSubjAsPercent($point, $maxpoint)
    {
        return round(($point / $maxpoint) * 100, 2);
    }

    public function scopeFilter ($builder, $filters)
    {
        return $filters->apply($builder);
    }

}
