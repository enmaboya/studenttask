<?php

namespace App;

use Illuminate\Http\Request;

class Filter
{
    protected $builder;
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }
        return $this->builder;
    }

    public function filters()
    {
        return $this->request->input();
    }

    //Группа
    public function title($value)
    {
        $this->builder->where('title', 'like', "%$value%");
    }

    public function desk($value)
    {
        $this->builder->where('description', 'like', "%$value%");
    }

    public function rus($value)
    {
        if (strlen($value) == 1) {
            $this->builder->where('avgrus', 'like', $value);
        } else {
            switch ($value[0]) {
                case '>':
                    $this->builder->where('avgrus', '>', $value[1]);
                    break;
                case '<':
                    $this->builder->where('avgrus', '<', $value[1]);
                    break;
            }
        }
    }

    public function math($value)
    {
        if (strlen($value) == 1) {
            $this->builder->where('avgmath', 'like', $value);
        } else {
            switch ($value[0]) {
                case '>':
                    $this->builder->where('avgmath', '>', $value[1]);
                    break;
                case '<':
                    $this->builder->where('avgmath', '<', $value[1]);
                    break;
            }
        }
    }

    public function hist($value)
    {
        if (strlen($value) == 1) {
            $this->builder->where('avghist', 'like', $value);
        } else {
            switch ($value[0]) {
                case '>':
                    $this->builder->where('avghist', '>', $value[1]);
                    break;
                case '<':
                    $this->builder->where('avghist', '<', $value[1]);
                    break;
            }
        }
    }

    // Студент
    public function fio($value)
    {
        $this->builder->where('name', 'like', "%$value%");
    }

    public function birt($value)
    {
        $this->builder->where('birthday', 'like', "%$value%");
    }

    public function group($value)
    {
        if ($value != 'Выберите группу') {
            $this->builder->where('group_id', 'like', "$value");
        }
    }

    public function russtud($value)
    {
        if ($value != null) {
            $this->builder->whereHas('ratings', function ($query) use ($value) {
                $query->where('points', 'like', $value);
                $query->where('subject_id', 'like', '1');
            });
        }
    }

    public function mathstud($value)
    {
        if ($value != null) {
            $this->builder->whereHas('ratings', function ($query) use ($value) {
                $query->where('points', 'like', $value);
                $query->where('subject_id', 'like', '2');
            });
        }
    }

    public function histstud($value)
    {
        if ($value != null) {
            $this->builder->whereHas('ratings', function ($query) use ($value) {
                $query->where('points', 'like', $value);
                $query->where('subject_id', 'like', '3');
            });
        }
    }
    public function mathSt($value)
    {
        $this->builder->where('group_id', 'like', "$value");
    }

    public function histSt($value)
    {
        $this->builder->where('group_id', 'like', "$value");
    }

    public function sort($value)
    {
        $col = substr($value, 3, 1);
        $sort = substr($value, 0, 3);
        if ($sort == 'des') $sort = 'desk';
        switch ($col) {
            case '0':
                $this->builder->orderBy('title', $sort);
                break;
            case '1':
                $this->builder->orderBy('description', $sort);
                break;
            case '3':
                $this->builder->orderBy('avgrus', $sort);
                break;
            case '4':
                $this->builder->orderBy('avgmath', $sort);
                break;
            case '5':
                $this->builder->orderBy('avghist', $sort);
                break;
        }
    }

    public function studsort ($value)
    {
        $col = substr($value, 3, 1);
        $sort = substr($value, 0, 3);
        if ($sort == 'des') $sort = 'desk';
        switch ($col) {
            case '0':
                $this->builder->orderBy('name', $sort);
                break;
            case '1':
                $this->builder->orderBy('birthday', $sort);
                break;
            case '2':
                $this->builder
                    ->join('groups', 'groups.id', '=', 'students.group_id')
                    ->select('groups.title', 'students.*')
                    ->orderBy('groups.title', $sort);
                break;
            case '4':
                $this->builder
                    ->join('ratings', 'students.id', '=', 'ratings.student_id')
                    ->select('students.*', 'ratings.subject_id', 'ratings.points')
                    ->where('ratings.subject_id', '=', 1)
                    ->orderBy('ratings.points', $sort);
                break;
            case '5':
                $this->builder
                    ->join('ratings', 'students.id', '=', 'ratings.student_id')
                    ->select('students.*', 'ratings.subject_id', 'ratings.points')
                    ->where('ratings.subject_id', '=', 2)
                    ->orderBy('ratings.points', $sort);
                break;
            case '6':
                $this->builder
                    ->join('ratings', 'students.id', '=', 'ratings.student_id')
                    ->select('students.*', 'ratings.subject_id', 'ratings.points')
                    ->where('ratings.subject_id', '=', 3)
                    ->orderBy('ratings.points', $sort);
                break;
        }
    }
}