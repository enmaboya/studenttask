<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Group;
use App\Models\Student;
use App\Policies\AdminGroupPolicy;
use App\Policies\AdminStudentPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Group::class => AdminGroupPolicy::class,
        Student::class => AdminStudentPolicy::class,
    ];

    public function boot()
    {
        $this->registerPolicies();
    }
}
