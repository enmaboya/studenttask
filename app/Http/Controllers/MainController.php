<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $groups = Group::with('students.ratings')->paginate(1);
        return view('main.index', compact('groups'));
    }
}
