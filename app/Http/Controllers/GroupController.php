<?php

namespace App\Http\Controllers;

use App\Filter;
use App\Models\Group;
use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Requests\GroupRequests\GroupCreateRequest;
use App\Http\Requests\GroupRequests\GroupEditRequest;

class GroupController extends Controller
{

    public function index(Request $request, Filter $filters)
    {
        $subjCount = Subject::count();
        $groups = Group::with('students.ratings')
            ->filter($filters)
            ->with('ratings')
            ->paginate(3)
        ->appends(
            $request->only(['title', 'desk', 'rus', 'math', 'hist', 'sort']));
        return view('groups.index', compact('groups', 'subjCount'));
    }

    public function create(Request $request)
    {
        $group = new Group;
        $this->authorize('create', $group);
        return view('groups.create');
    }

    public function store(GroupCreateRequest $request)
    {
        Group::create($request->all());
        return redirect(route('groups.index'));
    }

    public function show(Group $group, Request $request)
    {
        $this->authorize('show', $group);
        return view('groups.show', compact('group'));
    }

    public function edit(Group $group, GroupEditRequest $request)
    {
        $group->update($request->all());
        return redirect(route('groups.index'));
    }

    public function destroy(Group $group, Request $request)
    {
        $this->authorize('delete', $group);
            foreach ($group->students as $student) {
                if ($student->imgpath != null) {
                    Storage::disk('public')->delete($student->imgpath);
                }
                $student->delete;
            }
        $group->delete();
        return back();
    }

    public function viewStudents(Group $group)
    {
        return view('groups.studlist', compact('group'));
    }
}
