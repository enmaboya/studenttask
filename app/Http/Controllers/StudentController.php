<?php

namespace App\Http\Controllers;

use Storage;
use App\Filter;
use App\Models\Group;
use App\Models\Rating;
use App\Models\Subject;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequests\StudentCreateRequest;
use App\Http\Requests\StudentRequests\StudentEditRequest;
use App\Http\Requests\StudentRequests\StudentImageRequest;

class StudentController extends Controller
{
    public function index(Request $request, Filter $filters)
    {
        $subjCount = Subject::count();
        //$groups = Group::all();

        $students = Student::with('ratings', 'group')
            ->filter($filters)
            ->paginate(3)
        ->appends(
            $request->only(['fio','birt', 'group', 'russtud', 'mathstud', 'histstud', 'studsort']));
        return view('students.index', compact('students', 'subjCount'));
    }

    public function create(Request $request)
    {
        $groups = Group::all();
        $student = new Student;

        $this->authorize('show', $student);
        return view('students.create', compact('groups'));
    }

    public function store(StudentCreateRequest $request)
    {
        $student = Student::create($request->all());
        Rating::create(['points' => $request->input('rating-rus'), 'student_id' => $student->id]);
        Rating::create(['points' => $request->input('rating-math'), 'student_id' => $student->id]);
        Rating::create(['points' => $request->input('rating-hist'), 'student_id' => $student->id]);
        return redirect('/students');
    }

    public function show(Student $student, Request $request)
    {
        $groups = Group::all();

        $this->authorize('show', $student);
        return view('students.show', compact('student', 'groups'));
    }

    public function edit(Student $student, StudentEditRequest $request)
    {
        $student->update($request->all());
        return redirect(route('students.index'));
    }

    public function destroy(Student $student, Request $request)
    {
        $this->authorize('delete', $student);
        if ($student->imgpath != null) {
            Storage::disk('public')->delete($student->imgpath);
        }
        $student->delete();
        return back();
    }

    public function upload(Student $student, StudentImageRequest $request)
    {
        if ($student->imgpath != null) {
            Storage::disk('public')->delete($student->imgpath);
        }
        $groups = Group::all();
        $path = $request->file('image')->store('uploads', 'public');

        $student->imgpath = $path;
        $student->save();
        return back();
    }
}
