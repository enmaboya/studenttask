<?php

namespace App\Http\Requests\StudentRequests;

use Illuminate\Foundation\Http\FormRequest;

class StudentCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'group_id' => 'required',
            'name' =>'required|string|max:50',
            'birthday' => 'required|date',
            'rating-rus' => 'required|numeric|max:5',
            'rating-math' => 'required|numeric|max:5',
            'rating-hist' => 'required|numeric|max:5',
        ];
    }
}
